import mirrorCreator from 'mirror-creator';

export const actionTypes = mirrorCreator([
  'MAP_SET',
  'MAP_RESET',
  'MAP_LOAD_SUCCESS',
  'MAP_LOAD_FAILURE',
]);

export const GOOGLE_LAYER_TYPES = {
  ROADMAP: 'roadmap',
  SATELLITE: 'satellite',
  TERRAIN: 'terrain',
  HYBRID: 'hybrid',
};

export const MAPBOX_LAYER_TYPES = {
  STREETS: 'mapbox.streets',
  SATELLITE: 'mapbox.satellite',
};
