import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createHistory from 'history/createBrowserHistory';
import thunkMiddleware from 'redux-thunk';
import { reducer as notificationsReducer } from 'react-notification-system-redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { reducers, initialState } from '../reducers';

export const history = createHistory();

const rootReducer = combineReducers({
  ...reducers,
  router: connectRouter(history),
  notifications: notificationsReducer,
});

const persistConfig = {
  key: 'telnetviewer190204',
  storage,
  whitelist: [
    'map',
  ],
};

const reducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(
    applyMiddleware(
      routerMiddleware(history),
      thunkMiddleware,
    ),
  ),
);

export const persistor = persistStore(store);
