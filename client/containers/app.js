import { Container } from 'reactstrap';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Route, Redirect, Switch } from 'react-router-dom';
import Dimensions from 'react-dimensions';

import Notifications from 'react-notification-system-redux';

import ErrorBoundaryApp from '../components/error_boundary_app';

import MapShow from './map_show';

// bootstrap
require('bootstrap/dist/css/bootstrap.css');

// local
require('../css/local.css');

class App extends Component {
  constructor(props) {
    super(props);
    this.renderMapShow = this.renderMapShow.bind(this);
  }

  renderMapShow(props) {
    const {
      containerWidth,
      containerHeight,
    } = this.props;
    return (
      <MapShow containerWidth={containerWidth} containerHeight={containerHeight} {...props} />
    );
  }

  renderNotifications() {
    const {
      notifications,
    } = this.props;
    return (
      <Notifications
        notifications={notifications}
      />
    );
  }

  render() {
    const {
      containerHeight,
    } = this.props;
    return (
      <ErrorBoundaryApp containerHeight={containerHeight}>
        <div>
          <Container fluid>
            <Switch>
              <Redirect exact from="/" to="/map" />
              <Route path="/map" render={this.renderMapShow} />
            </Switch>
          </Container>
          { this.renderNotifications() }
        </div>
      </ErrorBoundaryApp>
    );
  }
}

function mapStateToProps(state) {
  return {
    notifications: state.notifications,
  };
}

export default compose(
  Dimensions(),
  connect(mapStateToProps, {}),
)(App);
