import { Table, Button, ButtonGroup } from 'reactstrap';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  mapSet,
} from '../actions/map_actions';

class MapData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handlePauseClick = this.handlePauseClick.bind(this);
  }

  handlePauseClick() {
    const {
      map: {
        isPolling,
      },
      mapSet: propsMapSet,
    } = this.props;
    propsMapSet({ isPolling: !isPolling });
  }

  render() {
    const {
      pane2height,
      map: {
        data,
        isPolling,
        loadTimeStamp,
      },
    } = this.props;
    const localeTimeStamp = (new Date(loadTimeStamp)).toLocaleString();
    return (
      <div
        id="data"
        style={{
          padding: '2em',
          overflowY: 'scroll',
          height: `${pane2height}px`,
        }}
      >
        <div>
          <ButtonGroup>
            <Button
              type="button"
              color="primary"
              onClick={this.handlePauseClick}
            >
              { isPolling ? 'Pause Polling' : 'Resume Polling' }
            </Button>
            <Button
              style={{ marginLeft: '1em' }}
              type="button"
              color="primary"
              outline
              disabled
            >
              <span style={{ paddingLeft: '0.5em', paddingRight: '0.5em' }}>{localeTimeStamp}</span>
            </Button>
          </ButtonGroup>
          <Table striped>
            <thead>
              <tr>
                <th>cowg</th>
                <th>left side volume</th>
                <th>intensity</th>
                <th>abs heading</th>
                <th>shock count</th>
                <th>speed</th>
                <th>collar</th>
                <th>right side volume</th>
                <th>distance</th>
                <th>zone</th>
                <th>time</th>
                <th>nudge state</th>
                <th>r heading</th>
              </tr>
            </thead>
            <tbody>
              {
                data
                && data.map((feature, index) => {
                  const {
                    cowg,
                    left_side_volume: leftSideVolume,
                    intensity,
                    abs_heading: absHeading,
                    shock_count: shockCount,
                    speed,
                    collar,
                    right_side_volume: rightSideVolume,
                    distance,
                    zone,
                    time,
                    nudge_state: nudgeState,
                    r_heading: rHeading,
                  } = feature;
                  return (
                    <tr key={index}>
                      <td>{cowg}</td>
                      <td>{leftSideVolume}</td>
                      <td>{intensity}</td>
                      <td>{absHeading}</td>
                      <td>{shockCount}</td>
                      <td>{speed}</td>
                      <td>{collar}</td>
                      <td>{rightSideVolume}</td>
                      <td>{distance}</td>
                      <td>{zone}</td>
                      <td>{time}</td>
                      <td>{nudgeState}</td>
                      <td>{rHeading}</td>
                    </tr>
                  );
                })
              }
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

MapData.propTypes = {
  pane2height: PropTypes.number.isRequired,
  mapSet: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  map: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  map: state.map,
});

export default connect(
  mapStateToProps,
  { mapSet },
)(MapData);
