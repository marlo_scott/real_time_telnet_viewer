import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SplitPane from 'react-split-pane';

import _debounce from 'lodash/debounce';

import MapLeaflet from './map_leaflet';
import MapData from './map_data';

import {
  mapLoad,
} from '../actions/map_actions';

require('leaflet/dist/leaflet.css');
// require('leaflet-draw/dist/leaflet.draw.css');
require('leaflet-geosearch/assets/css/leaflet.css');

require('../css/Leaflet.Control.ZoomDisplay.css');

require('../css/map.scss');

require('../css/react-split-pane.css');

class MapShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pane1height: (props.containerHeight - 10) / 2,
      pane2height: (props.containerHeight - 10) / 2,
    };
    this.handleResizeChange = this.handleResizeChange.bind(this);
  }

  componentWillMount() {
    this.delayedHandleResizeChange = _debounce((size) => {
      const {
        containerHeight,
      } = this.props;
      this.setState({
        pane1height: size,
        pane2height: (containerHeight - 10) - size
      });
    }, 100);
  }

  componentDidMount() {
    const {
      mapLoad: propsMapLoad,
    } = this.props;
    this.intervalMapLoad = setInterval(() => {
      const {
        map: {
          isPolling,
        },
      } = this.props;
      if (isPolling) {
        propsMapLoad();
      }

    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalMapLoad);
    this.intervalMapLoad = null;
    this.delayedHandleResizeChange.cancel();
  }

  handleResizeChange(size) {
    this.delayedHandleResizeChange(size);
  }

  render() {
    const { containerHeight } = this.props;
    const { pane1height, pane2height } = this.state;
    return (
      <div className="container-fluid-map">
        <SplitPane
          split="horizontal"
          minSize={200}
          defaultSize={(containerHeight - 10) / 2}
          onChange={this.handleResizeChange}
        >
          <MapLeaflet pane1height={pane1height} />
          <MapData pane2height={pane2height} />
        </SplitPane>
      </div>
    )
  }
}

MapShow.propTypes = {
  containerHeight: PropTypes.number.isRequired,
  mapLoad: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  map: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  map: state.map,
});

export default connect(
  mapStateToProps,
  {
    mapLoad,
  },
)(MapShow);
