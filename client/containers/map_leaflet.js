import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import L from 'leaflet';
import { GeoSearchControl, GoogleProvider } from 'leaflet-geosearch';

import 'leaflet.gridlayer.googlemutant/Leaflet.GoogleMutant';

import _debounce from 'lodash/debounce';
import _findIndex from 'lodash/findIndex';
import _includes from 'lodash/includes';
import _isNaN from 'lodash/isNaN';
import _merge from 'lodash/merge';
import _reverse from 'lodash/reverse';

import {
  GOOGLE_LAYER_TYPES,
  MAPBOX_LAYER_TYPES,
} from '../constants';

import {
  mapSet,
} from '../actions/map_actions';

// GK Halter-Dev
const GOOGLE_API_TOKEN = 'AIzaSyB4Edm7OtSV6h28hHoePlXV8UW4UiEMeNo';
// GK Halter-Dev
const MAPBOX_API_TOKEN = 'pk.eyJ1IjoiZ29yZG9ua2luZzAyIiwiYSI6ImNqb2kwZW5mczA0NHgza3Q2Ymd4Z2Q2OTAifQ.3dl9vow5mJdlU4-JLijKFA';

const baseLayerTemplates = [
  {
    name: 'Google Maps - Hybrid', type: 'google', maptype: GOOGLE_LAYER_TYPES.HYBRID, url: null,
  },
  {
    name: 'Google Maps - Satellite', type: 'google', maptype: GOOGLE_LAYER_TYPES.SATELLITE, url: null,
  },
  {
    name: 'Google Maps - Roadmap', type: 'google', maptype: GOOGLE_LAYER_TYPES.ROADMAP, url: null,
  },
  {
    name: 'Google Maps - Terrain', type: 'google', maptype: GOOGLE_LAYER_TYPES.TERRAIN, url: null,
  },
  {
    name: 'Open Street Map - Roadmap', type: 'osm', url: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png', attribution: 'OpenStreetMap',
  },
  {
    name: 'Mapbox - Street', type: 'mapbox', maptype: MAPBOX_LAYER_TYPES.STREETS, url: `https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=${MAPBOX_API_TOKEN}`, attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  },
  {
    name: 'Mapbox - Satellite', type: 'mapbox', maptype: MAPBOX_LAYER_TYPES.SATELLITE, url: `https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=${MAPBOX_API_TOKEN}`, attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  },
];

L.Icon.Default.imagePath = '//cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/';

const FEATURE_FLAG_FARM_TILES = false;

class MapLeaflet extends Component {
  constructor(props) {
    super(props);
    this.handleMapMoveend = this.handleMapMoveend.bind(this);
    this.handleBaseLayerChange = this.handleBaseLayerChange.bind(this);
  }

  componentWillMount() {
    this.delayedHandleMapMoveend = _debounce(() => {
      const {
        mapSet: propsMapSet,
      } = this.props;
      const mapCenter = this.map.getCenter();
      const mapZoom = this.map.getZoom();
      const mapData = {
        lat: mapCenter.lat,
        lng: mapCenter.lng,
        zoom: mapZoom,
      };
      propsMapSet(mapData);
    }, 100);
  }

  componentDidMount() {
    const {
      map,
    } = this.props;
    this.map = L.map('map', {
      center: [map.lat, map.lng],
      zoom: map.zoom,
      zoomControl: false,
    })
      .on('moveend', this.handleMapMoveend)
      .on('baselayerchange', this.handleBaseLayerChange);

    L.control.zoom({ position: 'bottomright' }).addTo(this.map);
    // eslint-disable-next-line global-require
    require('../lib/Leaflet.Control.ZoomDisplay');
    L.control.zoomDisplay({ position: 'bottomright' }).addTo(this.map);
    L.control.scale({ position: 'bottomleft' }).addTo(this.map);
    new GeoSearchControl({
      position: 'bottomright',
      retainZoomLevel: false,
      animateZoom: false,
      autoClose: true,
      provider: new GoogleProvider({ params: { key: GOOGLE_API_TOKEN } }),
    }).addTo(this.map);

    let overlayMaps = {};
    if (FEATURE_FLAG_FARM_TILES) {
      this.farmLayer = L.tileLayer(
        './tiles/{z}/{x}/{y}.png',
        {
          tms: true,
          opacity: 0.7,
          attribution: `${new Date().getFullYear()} Halter USA Inc. All rights reserved.`,
          minZoom: 16,
          maxZoom: 23,
        },
      );
      overlayMaps = { Aerial: this.farmLayer };
    }
    this.baseLayers = baseLayerTemplates.map(baseLayer => this.createBaseLayer(baseLayer));
    const baseLayersMenu = this.baseLayers.reduce((result, layer) => (
      _merge({}, result, { [layer.name]: layer.element })
    ), {});
    this.layersControl = L.control.layers(baseLayersMenu, overlayMaps, { position: 'bottomleft' });
    this.layersControl.addTo(this.map);
    baseLayersMenu[this.baseLayers[map.baseLayerIndex].name].addTo(this.map);

    this.editableLayers = new L.GeoJSON(null, {
      onEachFeature: (feature, layer) => {
        layer.on('add', (e) => {
          const addLayer = e.target;
          addLayer.bindTooltip(this.setTooltipName(addLayer), { direction: 'center' });
        });
      },
      pointToLayer: (feature, latlng) => {
        const iconUrl = `data:image/svg+xml;base64,${btoa(this.makeSvg(feature))}`;
        const icon = L.icon({
          iconUrl,
          iconSize: [72, 72],
          iconAnchor: [36, 36],
          tooltipAnchor: [0, 36],
        });
        return L.marker(latlng, { icon });
        // return L.marker(latlng);
      },
    });
    this.map.addLayer(this.editableLayers);
    this.redrawMap();
  }

  componentDidUpdate(prevProps) {
    const {
      map,
      pane1height,
    } = this.props;
    if (prevProps.map.baseLayerIndex !== map.baseLayerIndex) {
      if (this.map.hasLayer(this.baseLayers[prevProps.map.baseLayerIndex].element)) {
        this.map.removeLayer(this.baseLayers[prevProps.map.baseLayerIndex].element);
        this.map.addLayer(this.baseLayers[map.baseLayerIndex].element);
      }
    }
    if (
      (prevProps.map.zoom !== map.zoom && map.zoom !== this.map.getZoom())
      || (prevProps.map.lat !== map.lat && map.lat !== this.map.getCenter().lat)
      || (prevProps.map.lng !== map.lng && map.lng !== this.map.getCenter().lng)
    ) {
      this.map.setView([map.lat, map.lng], map.zoom);
    }
    if (
      (prevProps.map.rebased && !map.rebased)
    ) {
      this.redrawMap();
    }
    if (
      (prevProps.pane1height !== pane1height)
    ) {;
      this.map.invalidateSize()
      this.redrawMap();
    }
  }

  componentWillUnmount() {
    this.delayedHandleMapMoveend.cancel();
    this.map.remove();
    this.map = null;
  }

  setTooltipName = (layer) => {
    const {
      name,
    } = layer.feature.properties;
    return name;
  }

  createBaseLayer = (baseLayer) => {
    const {
      type,
      maptype,
      url,
      attribution,
    } = baseLayer;
    let element;
    if (type === 'google') {
      element = L.gridLayer.googleMutant({ type: maptype });
    } else if (type === 'osm') {
      element = L.tileLayer(url, { attribution });
    } else if (type === 'mapbox') {
      element = L.tileLayer(url, { attribution, id: maptype, accessToken: MAPBOX_API_TOKEN });
    }
    return Object.assign({}, baseLayer, { element });
  }

  makeFeature = (data) => {
    const {
      cowg,
      collar,
      zone,
      gps_position: gpsPosition,
      left_side_volume: leftSideVolume,
      right_side_volume: rightSideVolume,
      abs_heading: absHeading,
    } = data;
    return {
      type: 'Feature',
      properties: {
        name: `Z:${zone}-C:${collar}`,
        type: 'collar',
        cowg,
        zone,
        collar,
        leftSideVolume,
        rightSideVolume,
        absHeading,
      },
      geometry: {
        type: 'Point',
        coordinates: _reverse(JSON.parse(gpsPosition)),
      },
    };
  }

  getSvgColor = (feature) => {
    const {
      properties: {
        cowg,
        zone,
      },
    } = feature;
    if (cowg === '3') {
      return 'red';
    }
    switch (zone) {
      case 'false':
        return 'orange';
      case '5':
        return 'green';
      case '6':
      case '8':
        switch (cowg) {
          case '0':
            return 'green';
          case '1':
            return 'white';
          default:
            return 'black';
        }
      case '7':
        switch (cowg) {
          case '0':
            return 'green';
          case '1':
            return 'yellow';
          default:
            return 'black';
        }
      default:
        return 'black';
    }
  }


  makeSvg = (feature) => {
    let textColor = 'white';
    const color = this.getSvgColor(feature);
    if (_includes(['white', 'orange', 'yellow'], color)) {
      textColor = 'black';
    }

    const {
      properties: {
        collar,
        absHeading,
        leftSideVolume,
        rightSideVolume,
      },
    } = feature;

    let pathAbs;
    let absHeadingInt = 0;
    let pathLeft;
    let pathRight;

    if (!_isNaN(parseInt(absHeading, 10))) {
      absHeadingInt = parseInt(absHeading, 10);
      const dAbs = this.describeArc(36, 36, 36, absHeadingInt - 1, absHeadingInt + 1);
      pathAbs = `<path d='${dAbs}' fill='white' stroke='white' stroke-width='1' />`;
    }

    if (!_isNaN(parseFloat(leftSideVolume))) {
      let dLeft;
      const leftSideVolumeFloat = parseFloat(leftSideVolume);
      const startDeg = absHeadingInt - 60;
      const endDeg = absHeadingInt - 30;
      if (leftSideVolumeFloat > 0) {
        dLeft = this.describeArc(36, 36, 24, startDeg, endDeg);
        pathLeft = `${pathLeft}<path d='${dLeft}' fill='none' stroke='white' stroke-width='2'/>`;
      }
      if (leftSideVolumeFloat > 0.33) {
        dLeft = this.describeArc(36, 36, 30, startDeg, endDeg);
        pathLeft = `${pathLeft}<path d='${dLeft}' fill='none' stroke='white' stroke-width='2'/>`;
      }
      if (leftSideVolumeFloat > 0.66) {
        dLeft = this.describeArc(36, 36, 36, startDeg, endDeg);
        pathLeft = `${pathLeft}<path d='${dLeft}' fill='none' stroke='white' stroke-width='2'/>`;
      }
    }

    if (!_isNaN(parseFloat(rightSideVolume))) {
      let dRight;
      const rightSideVolumeFloat = parseFloat(rightSideVolume);
      const startDeg = absHeadingInt + 30;
      const endDeg = absHeadingInt + 60;
      if (rightSideVolumeFloat > 0) {
        dRight = this.describeArc(36, 36, 24, startDeg, endDeg);
        pathRight = `${pathRight}<path d='${dRight}' fill='none' stroke='white' stroke-width='2'/>`;
      }
      if (rightSideVolumeFloat > 0.33) {
        dRight = this.describeArc(36, 36, 30, startDeg, endDeg);
        pathRight = `${pathRight}<path d='${dRight}' fill='none' stroke='white' stroke-width='2'/>`;
      }
      if (rightSideVolumeFloat > 0.66) {
        dRight = this.describeArc(36, 36, 36, startDeg, endDeg);
        pathRight = `${pathRight}<path d='${dRight}' fill='none' stroke='white' stroke-width='2'/>`;
      }
    }

    const svg = `\
      <svg xmlns='http://www.w3.org/2000/svg' width='72' height='72'>\
        ${pathAbs}\
        ${pathLeft}\
        ${pathRight}\
        <circle cx='36' cy='36' r='18' stroke='${color}' stroke-width='1' fill='${color}' />\
        <text x='50%' y='50%' text-anchor='middle' stroke='${textColor}' stroke-width='1px' dy='.3em'>${collar}</text>\
      </svg>\
    `;
    return svg;
  }

  polarToCartesian = (centerX, centerY, radius, angleInDegrees) => {
    const angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;
    return {
      x: centerX + (radius * Math.cos(angleInRadians)),
      y: centerY + (radius * Math.sin(angleInRadians)),
    };
  }

  describeArc = (x, y, radius, startAngle, endAngle) => {
    const start = this.polarToCartesian(x, y, radius, endAngle);
    const end = this.polarToCartesian(x, y, radius, startAngle);
    const arcSweep = endAngle - startAngle <= 180 ? '0' : '1';
    const d = [
      'M', start.x, start.y,
      'A', radius, radius, 0, arcSweep, 0, end.x, end.y,
      'L', x, y,
      'L', start.x, start.y,
    ].join(' ');

    return d;
  }

  redrawMap() {
    const {
      map,
      mapSet: propsMapSet,
    } = this.props;

    this.editableLayers.clearLayers();

    if (map.data.length > 0) {
      map.data.forEach((feature) => {
        this.editableLayers.addData(this.makeFeature(feature));
      });
    }

    propsMapSet({
      rebased: true,
    });

  }

  handleBaseLayerChange(e) {
    const {
      mapSet: propsMapSet,
    } = this.props;
    const baseLayerIndex = _findIndex(this.baseLayers, { name: e.name });
    if (baseLayerIndex > -1) {
      propsMapSet({ baseLayerIndex });
    }
  }

  handleMapMoveend(e) {
    this.delayedHandleMapMoveend(e);
  }

  render() {
    const {
      pane1height,
    } = this.props;
    return (
       <div id="map" style={{ flex: 1, height: `${pane1height}px` }} />
    );
  }
}

MapLeaflet.propTypes = {
  pane1height: PropTypes.number.isRequired,
  mapSet: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  map: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  map: state.map,
});

export default connect(
  mapStateToProps,
  { mapSet },
)(MapLeaflet);
