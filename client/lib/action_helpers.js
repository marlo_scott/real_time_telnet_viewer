import * as Notifications from 'react-notification-system-redux';
import { push } from 'connected-react-router';
import _get from 'lodash/get';

function notifyArgs(title, message = undefined) {
  const args = {
    title,
    position: 'bl',
    autoDismiss: 6,
  };
  if (message) {
    args.message = message;
  }
  return args;
}

export const notifySuccessDev = (dispatch, title, message = undefined) => {
  if (window.$NODE_ENV === 'development') {
    dispatch(Notifications.success(notifyArgs(title, message)));
  }
};

export const notifySuccess = (dispatch, title, message = undefined) => {
  dispatch(Notifications.success(notifyArgs(title, message)));
};

export const notifyError = (dispatch, title, message = undefined) => {
  dispatch(Notifications.error(notifyArgs(title, message)));
};

export const manageCatch = (dispatch, res, failure) => {
  const err = { ...res }.response;
  if (window.$NODE_ENV === 'development') {
    // eslint-disable-next-line no-console
    console.log(err);
  }
  if (err) {
    if (err.status === 401) {
      notifyError(dispatch, err.statusText);
      dispatch(push('/logout'));
    } else {
      notifyError(dispatch, err.statusText, _get(err, 'data.errors.message', 'Something went wrong'));
      if (failure) {
        dispatch(failure());
      }
    }
  }
};
