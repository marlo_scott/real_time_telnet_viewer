import axios from 'axios';

import { actionTypes } from '../constants';
import { manageCatch, notifySuccessDev } from '../lib/action_helpers';

import * as stubData from '../fixtures/data.json';

export function mapReset() {
  return {
    type: actionTypes.MAP_RESET,
  };
}

export function mapSet(payload) {
  return {
    type: actionTypes.MAP_SET,
    payload,
  };
}

function mapLoadSuccess(payload, meta) {
  return {
    type: actionTypes.MAP_LOAD_SUCCESS,
    payload,
    meta,
  };
}

function mapLoadFailure() {
  return {
    type: actionTypes.MAP_LOAD_FAILURE,
  };
}

export function mapLoad(success = mapLoadSuccess, failure = mapLoadFailure) {
  return function mapLoadDispatch(dispatch) {
    return axios({
      method: 'get',
      url: 'http://localhost:8080/collarManager/read_data',
    })
      .then((resp) => {
        let {
          data,
        } = resp;
        if (data.length === 0) {
          data = stubData.default;
        }
        dispatch(success({ data }));
        // notifySuccessDev(dispatch, 'Map loaded');
        return resp;
      })
      .catch((res) => {
        manageCatch(dispatch, res, failure);
      });
  };
}
