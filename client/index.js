// import React from 'react'
// import ReactDOM from 'react-dom'

// const App = (props) => {
//   return <p>Hello azure</p>
// }

// const rootEl = document.getElementById('root')

// function render (Component) {
//   ReactDOM.render(
//     <App />,
//     rootEl
//   )
// }
// render(App)
import 'whatwg-fetch';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';

import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import { PersistGate } from 'redux-persist/es/integration/react';

import Loader from './components/loader';
import { store, history, persistor } from './store';
import App from './containers/app';

const rootEl = document.getElementById('root');

const renderApp = Component => (
  <AppContainer>
    <Provider store={store}>
      <PersistGate
        loading={<Loader />}
        persistor={persistor}
      >
        <ConnectedRouter history={history}>
          <Route path="/" component={Component} />
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  </AppContainer>
);

render(renderApp(App), rootEl);

if (module.hot) {
  module.hot.accept('./containers/app', () => {
    // eslint-disable-next-line global-require
    const NextApp = require('./containers/app').default;
    render(renderApp(NextApp), rootEl);
  });
}
