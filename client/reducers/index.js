import mapReducer, { initialState as mapInitialState } from './map_reducer';

export const reducers = {
  map: mapReducer,
};

export const initialState = {
  map: mapInitialState,
};
