import { actionTypes } from '../constants';

const initialMapCoodinates = process.env.INITIAL_MAP_COORDINATES
  ? JSON.parse(process.env.INITIAL_MAP_COORDINATES)
  : {
    lat: -37.688234,
    lng: 175.59216,
    zoom: 16,
  };

export const initialState = {
  ...initialMapCoodinates,
  defaultLat: initialMapCoodinates.lat,
  defaultLng: initialMapCoodinates.lng,
  baseLayerIndex: 0,
  maxNativeZoom: 18,
  minZoom: 7,
  maxZoom: 22,
  data: [],
  polling: true,
  loadTimeStamp: '',
};

function rehydrate(state, payload) {
  return payload && Object.keys(payload).length > 0 && Object.keys(payload).includes('map') ? { ...initialState, ...payload.map } : initialState;
}

export default function mapReducer(state = initialState, action) {
  const { type, payload, error } = action;
  if (error) {
    return state;
  }

  switch (type) {
    case actionTypes.MAP_LOAD_SUCCESS:
      return { ...state, ...payload, ...{ rebased: false, loadTimeStamp: (new Date()).toISOString() } };
    case actionTypes.MAP_RESET:
      return initialState;
    case actionTypes.MAP_SET:
      return { ...state, ...payload };
    case 'persist/REHYDRATE':
      return rehydrate(state, payload);
    default:
      return state;
  }
}
