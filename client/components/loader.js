import React from 'react';
import Spinner from 'react-spinkit';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

function Loader({
  wrapperStyle,
  noSpinner,
  fadeIn,
  name,
  color,

}) {
  const defaultWrapperStyle = {
    zIndex: 1000,
    position: 'absolute',
    left: 0,
    top: 0,
    backgroundColor: 'rgba(252,252,252,0.1)',
    width: '100%',
    height: '100%',
  };

  return (
    <div style={wrapperStyle || defaultWrapperStyle}>
      <div style={{
        position: 'relative', top: '50%', left: '50%', width: '32px', height: '32px',
      }}
      >
        {
          noSpinner
            || (
            <Spinner
              fadeIn={fadeIn || 'half'}
              name={name || 'circle'}
              color={color || '#bcbcbc'}
            />
            )
        }
      </div>
    </div>
  );
}

Loader.propTypes = {
  wrapperStyle: stylePropType,
  noSpinner: PropTypes.bool,
  fadeIn: PropTypes.string,
  name: PropTypes.string,
  color: PropTypes.string,
};

Loader.defaultProps = {
  wrapperStyle: null,
  noSpinner: false,
  fadeIn: 'half',
  name: 'circle',
  color: '#bcbcbc',
};

export default Loader;
