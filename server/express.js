/* eslint-disable */
import exitHook from 'exit-hook';
import express from 'express';

const webpack = require('webpack');
const historyApiFallback = require('connect-history-api-fallback');
const expressStaticGzip = require('express-static-gzip');

const config = require('../webpack/webpack.dev.js');
const compiler = webpack(config());
const port = process.env.PORT || 8000;
const app = express();
const webpackDevMiddleware = require('webpack-dev-middleware')(
  compiler,
  config.devServer,
);

const webpackHotMiddlware = require('webpack-hot-middleware')(
  compiler,
  config.devServer,
);

app.use(historyApiFallback({
  verbose: false,
}));

app.use(webpackDevMiddleware);
app.use(webpackHotMiddlware);

app.use(
  expressStaticGzip('dist', {
    enableBrotli: true,
  }),
);

app.listen(port, (error) => {
  if (error) {
    console.log(error);
  }
  console.info('==> Listening on port %s.  Node environment is %s', port, process.env.NODE_ENV);
});

exitHook(() => {
  console.log('Received kill signal. Shutting down');
});
